from django.apps import AppConfig


class CourrierConfig(AppConfig):
    name = 'courrier'
    def ready(self):
        import courrier.signals
