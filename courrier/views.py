from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.shortcuts import render, redirect

from .decorators import unauthenticated_user, admin_only, allowed_users
from .forms import OrderForm, createUserFrom, EmployeeForm, CourrierForm
from courrier.models import *
from .filters import OrderFilter, Filter, CourrierFilter
from .forms import OrderForm
from django.forms import inlineformset_factory


@login_required(login_url='login')
@admin_only
def page(request):

    orders= Order.objects.all()
    employees= Employee.objects.all()
    total_employee=employees.count()
    total_orders =orders.count()
    traiter = orders.filter(status='Traiter').count()
    encore = orders.filter(status='Encore').count()
    PasEncore = orders.filter(status='Pas encore').count()


    context= {'orders':orders,'employees':employees,'total_employee':total_employee,
              'traiter':traiter,'encore':encore,'total_orders':total_orders,'PasEncore':PasEncore }
    return render(request, 'gestoin/dashbord.html',context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def courriers(request):
    courriers = Courrier.objects.all()

    myFilter = CourrierFilter(request.GET, queryset=courriers)
    courriers = myFilter.qs
    return render(request, 'gestoin/courriers.html',{'courriers':courriers,'myFilter':myFilter})

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def employee(request,pk):
    employee = Employee.objects.get(id = pk)
    orders = employee.order_set.all()
    total_orders = orders.count()
    myFilter = OrderFilter(request.GET, queryset=orders)
    orders = myFilter.qs
    context = {'orders':orders,'employees': employee,'total_orders':total_orders,'myFilter':myFilter}
    return render(request, 'gestoin/employee.html',context)


@login_required(login_url='login')
def createOrder(request,pk):
    OrderFormSet = inlineformset_factory(Employee, Order, fields=('courrier', 'status'),extra=5)
    employee = Employee.objects.get(id=pk)
    form = OrderFormSet(queryset=Order.objects.none(),instance=employee)
    #form = OrderForm(initial={'employee':employee})
    if request.method == 'POST':
        #form = OrderForm(request.POST)
        form = OrderFormSet(request.POST,instance=employee)
        if form.is_valid():
            form.save()
            return redirect('home')
    context = {'form': form }
    return render(request, 'gestoin/order_form.html', context)




@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def updateOrder(request, pk):
	order = Order.objects.get(id=pk)
	form = OrderForm(instance=order)

	if request.method == 'POST':
		form = OrderForm(request.POST, instance=order)
		if form.is_valid():
			form.save()
			return redirect('home')

	context = {'form':form}
	return render(request, 'gestoin/order_form.html', context)









@login_required(login_url='login')
def deleteOrder(request, pk):
	order = Order.objects.get(id=pk)
	if request.method == "POST":
		order.delete()
		return redirect('home')

	context = {'item':order}
	return render(request, 'gestoin/delete.html', context)\

@login_required(login_url='login')
def deleteEmployee(request, pk):
	order = Employee.objects.get(id=pk)
	if request.method == "POST":
		order.delete()
		return redirect('home')

	context = {'item':order}
	return render(request, 'gestoin/delete_employee.html', context)

@unauthenticated_user
def registerPage(request):
    form =createUserFrom()
    if request.method == 'POST' :
        form = createUserFrom(request.POST)
        if form.is_valid():
            user=form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, 'Account was created for ' + username)
            return redirect('login')
    context = {'form':form}
    return render(request, 'gestoin/register.html',context)



@unauthenticated_user
def registerPage1(request):
    form =createUserFrom()
    if request.method == 'POST' :
        form = createUserFrom(request.POST)
        if form.is_valid():
            user=form.save()
            username = form.cleaned_data.get('username')


            messages.success(request, 'Account was created for ' + username)


            return redirect('login')
    context = {'form':form}
    return render(request, 'gestoin/registerAccount.html',context)





@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def createEmployee(request):
    form = createUserFrom()
    if request.method == 'POST':
        form = createUserFrom(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')

            messages.success(request, 'Account was created for ' + username)

            return redirect('home')
    context = {'form': form}
    return render(request, 'gestoin/createEmployeee.html', context)

@unauthenticated_user
def loginPage(request):
    if request.method == 'POST':

        urname = request.POST.get('username')
        pwd = request.POST.get('password')

        user = authenticate(request, username=urname, password=pwd)
        if user is not None:
            login(request, user)
            return redirect('home')
        else:
             messages.error(request, 'username or password is not correct ')


    context={}
    return render(request, 'gestoin/login.html', context)

def logoutPage(request):
    logout(request)
    return redirect('login')

@login_required(login_url='login')
@allowed_users(allowed_roles=['employee'])
def userPage(request):
    orders = request.user.employee.order_set.all()
    total_orders = orders.count()
    traiter = orders.filter(status='Traiter').count()
    encore = orders.filter(status='Encore').count()
    context = {"orders":orders, 'traiter':traiter,'encore':encore,'total_orders':total_orders}
    return render(request, 'gestoin/user.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['employee'])
def accountSetting(request):
    employee = request.user.employee
    form = EmployeeForm(instance=employee)
    if request.method == 'POST':
        form = EmployeeForm(request.POST,request.FILES,instance=employee)
        if form.is_valid():
            form.save()
            return redirect('home')

    context = {"form":form}
    return render(request, 'gestoin/account_setting.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def ajouterCourrier(request):
    form = CourrierForm(request.POST or None)
    context = {"fomr":form}
    if form.is_valid():
        instance= form.save(commit=False)
        instance.save()
        return redirect('courrier')
    return render(request , "gestoin/createCourrier.html",context)\




@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def deleteCourrier(request, pk):
	order = Courrier.objects.get(id=pk)
	if request.method == "POST":
		order.delete()
		return redirect('courrier')

	context = {'item':order}
	return render(request, 'gestoin/deleteCourrier.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def updateCourrier(request, pk):
	order = Courrier.objects.get(id=pk)
	form = CourrierForm(instance=order)

	if request.method == 'POST':
		form = CourrierForm(request.POST, instance=order)
		if form.is_valid():
			form.save()
			return redirect('home')

	context = {'form':form}
	return render(request, 'gestoin/order_from_update.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def updateEmployee(request, pk):
	order = Employee.objects.get(id=pk)
	form = EmployeeForm(instance=order)

	if request.method == 'POST':
		form = EmployeeForm(request.POST, instance=order)
		if form.is_valid():
			form.save()
			return redirect('home')

	context = {'form':form}
	return render(request, 'gestoin/updateEmployee.html', context)

def Homepage(request):
	return render(request, 'gestoin/login.html')


@login_required(login_url='login')
@allowed_users(allowed_roles=['employee'])
def test(request):
    employee = request.user.employee
    form = EmployeeForm(instance=employee)
    if request.method == 'POST':
        form = EmployeeForm(request.POST, request.FILES, instance=employee)
        if form.is_valid():
            form.save()
            return redirect('home')

    orders = request.user.employee.order_set.all()
    total_orders = orders.count()
    traiter = orders.filter(status='Traiter').count()
    encore = orders.filter(status='Encore').count()
    PasEncore = orders.filter(status='Pas encore').count()
    context = {"orders": orders, 'traiter': traiter,'PasEncore':PasEncore ,'form':form,'encore': encore, 'total_orders': total_orders}
    return render(request, 'gestoin/web.html', context)


