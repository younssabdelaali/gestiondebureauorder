# Generated by Django 3.0.7 on 2020-06-14 15:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('courrier', '0003_auto_20200611_2350'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='note',
        ),
    ]
