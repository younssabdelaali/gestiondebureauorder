import django_filters
from django_filters import *

from .models import *
class OrderFilter(django_filters.FilterSet):
	end_date = DateFilter(field_name="date_created", lookup_expr='lte')
	#status = CharFilter(field_name="status")
	class Meta:
		model = Order
		fields = '__all__'
		exclude = ['employee', 'date_created','note']

class CourrierFilter(django_filters.FilterSet):

	class Meta:
		model = Courrier
		fields = '__all__'
		exclude = ['objet', 'date_created','pieacejoint']


