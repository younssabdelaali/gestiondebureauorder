from django.forms import ModelForm
from .models import *
from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User


class EmployeeForm(ModelForm):
	class Meta:
		model = Employee
		fields = '__all__'
		exclude=['user']


class OrderForm(ModelForm):
    class Meta:
        model = Order
        fields = '__all__'
        widgets = {'date_created': forms.DateInput(format=('%Y-%m-%d %I:%M:%S %p'), attrs={'type': 'date'})}


class createUserFrom(UserCreationForm):
	class Meta:
		model = User
		fields = ['username','email','password1','password2']

class CourrierForm(ModelForm):
    class Meta :
        model = Courrier
        fields = '__all__'
        widgets = {'date_created': forms.DateInput(format=('%Y-%m-%d %I:%M:%S %p'), attrs={'type': 'date'})}




class DateInput(forms.DateInput):
    input_type = 'date'