from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.db import models

# Create your models here.

class Employee(models.Model):
    user = models.OneToOneField(User,null=True, blank=True,on_delete=models.CASCADE)
    name = models.CharField(max_length=200, null=True)
    phone = models.CharField(max_length=200, null=True)
    email = models.CharField(max_length=200, null=True)
    profile_pic = models.ImageField(default="login.jpg",null=True,blank=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.name



class Departement(models.Model):
    name = models.CharField(max_length=200, null=True)

    def __str__(self):
        return self.name



class Service(models.Model):
    name = models.CharField(max_length=200, null=True)
    def __str__(self):
        return self.name


class Courrier(models.Model):
    CATEGORY = (
        ('courrier depart', 'courrier depart'),
        ('courrier arrive', 'courrier arrive'),
    )
    DEPARTEMENT = (
        ('Biologie', 'Biologie'),
        ('Chimie', 'Chimie'),
        ('Informatique', 'Informatique'),
        ('Mathématiques', 'Mathématiques'),
        ('Physique', 'Physique'),
    )

    reference = models.CharField(max_length=20)
    nature = models.CharField(max_length=30)
    objet= models.CharField(max_length=30)
    category = models.CharField(max_length=200, null=True, choices=CATEGORY)
    departement = models.CharField(max_length=200, null=True, choices=DEPARTEMENT)
    date_created = models.DateField()
    pieacejoint = models.CharField(max_length=20)
    service = models.ForeignKey('Service', null=True, on_delete=models.SET_NULL)

    def __str__(self):
         return self.reference

class Order(models.Model):
    STATUS = (
        ('Traiter', 'Traiter'),
        ('Pas encore', 'Pasencore'),
        ('Encore', 'Encore'),
    )

    employee = models.ForeignKey('Employee', null=True, on_delete=models.SET_NULL)
    courrier = models.ForeignKey('Courrier', null=True, on_delete=models.SET_NULL)
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    status = models.CharField(max_length=200, null=True, choices=STATUS)


    def __str__(self):
        return self.courrier.reference
