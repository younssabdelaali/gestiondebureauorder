from django.urls import path
from django.contrib.auth import views as auth_views
from courrier import views

urlpatterns = [
    path('', views.page,name="home"),
    path('home/', views.Homepage,name="homePage"),

    path('courrier/', views.courriers,name="courrier"),
    path('login/', views.loginPage, name="login"),
    path('logout/', views.logoutPage, name="logout"),
    path('register/', views.registerPage, name="register"),
    path('registerAccount/', views.registerPage1, name="registerAccount"),
    #path('user/', views.userPage, name="user"),
    path('user/', views.test, name="test"),
    path('employee/<str:pk>', views.employee,name="employee"),
    path('create_order/<str:pk>', views.createOrder, name="createOrder"),
    path('create_employee/', views.createEmployee, name="createEmployee"),
    path('update_order/<str:pk>', views.updateOrder, name="updateOrder"),

    path('delete_order/<str:pk>', views.deleteOrder, name="deleteOrder"),
    path('delete_employee/<str:pk>', views.deleteEmployee, name="deleteEmployee"),
    path('delete_courrier/<str:pk>', views.deleteCourrier, name="deleteCourrier"),
    path('update_courrier/<str:pk>', views.updateCourrier, name="updateCourrier"),
    path('update_employee/<str:pk>', views.updateEmployee, name="updateEmployee"),
    path('account/', views.accountSetting, name="account"),
    path('create_courrier/', views.ajouterCourrier,name='formulaire'),



    path('reset_password/',
         auth_views.PasswordResetView.as_view(template_name="gestoin/password_reset.html"),
         name="reset_password"),

    path('reset_password_sent/',
         auth_views.PasswordResetDoneView.as_view(template_name="gestoin/password_reset_sent.html"),
         name="password_reset_done"),
    # user's id encoded in base 64  token : to checke password is valid

    path('reset/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name="gestoin/password_reset_form.html"),
         name="password_reset_confirm"),
#seccess message reset
    path('reset_password_complete/',
         auth_views.PasswordResetCompleteView.as_view(template_name="gestoin/password_reset_done.html"),
         name="password_reset_complete"),
]