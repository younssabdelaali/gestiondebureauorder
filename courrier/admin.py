from django.contrib import admin

# Register your models here.
from courrier.models import *

admin.site.register(Employee)
admin.site.register(Courrier)
admin.site.register(Order)
admin.site.register(Service)
admin.site.register(Departement)